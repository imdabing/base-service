# 程序员们的花店

#### Description
1、自我介绍：20岁毕业于北大青鸟，毕业后的7年时间混迹于各种不知名的互联网初创公司。擅长技术：增删改查；擅长工具：Ctrl+C、Ctrl+V；爱好：吐槽；性格：奇葩；成就：做过管理系统，做过电商系统，做过零售系统，做过智能硬件，做过区块链，做过大数据系统等等十几个系统，好像没几个上线运营的，运营了的没一个运营得长久的
2、代码介绍：运用最擅长的工具：Ctrl+C、Ctrl+V将热门开源项目改改，供急切需要增删改查的程序员们搭建脚手架；在这基础上，一起做一个有意思的产品。
3、仰望星空：往后余生，不想日复一日增删改查，不想年复一年Ctrl+C、Ctrl+V，不想再吐槽，不想再奇葩，想做一个有意思的产品，让它上线，让它运营。
4、产品介绍：希望每一个程序员都能送一个花店给喜欢的人--一个线上虚拟操作、线下获得鲜花的产品。
5、招募：JAVA、H5、Android、IOS、小程序、设计师、产品、摄影师、文案、采购、网络运营、渠道推广、等


#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)