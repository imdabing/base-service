# 程序员们的花店

#### 介绍
1、自我介绍：20岁毕业于北大青鸟，毕业后的7年时间混迹于各种不知名的互联网初创公司。擅长技术：增删改查；擅长工具：Ctrl+C、Ctrl+V；爱好：吐槽；性格：奇葩；成就：做过管理系统，做过电商系统，做过零售系统，做过智能硬件，做过区块链，做过大数据系统等等十几个系统，好像没几个上线运营的，运营了的没一个运营得长久的
2、代码介绍：运用最擅长的工具：Ctrl+C、Ctrl+V将热门开源项目改改，供急切需要增删改查的程序员们搭建脚手架；在这基础上，一起做一个有意思的产品。
3、仰望星空：往后余生，不想日复一日增删改查，不想年复一年Ctrl+C、Ctrl+V，不想再吐槽，不想再奇葩，想做一个有意思的产品，让它上线，让它运营。
4、产品介绍：希望每一个程序员都能送一个花店给喜欢的人--一个线上虚拟操作、线下获得鲜花的产品。
5、招募：JAVA、H5、Android、IOS、小程序、设计师、产品、摄影师、文案、采购、网络运营、渠道推广、等


#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)